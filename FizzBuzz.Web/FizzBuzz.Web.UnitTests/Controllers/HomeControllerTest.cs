﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.Web;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;

namespace FizzBuzz.Web.UnitTests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();
            FizzBuzzModel fizzBuzzModel = new FizzBuzzModel { InputNumber = 10, Records = null };

            // Act
            ViewResult result = controller.Index(fizzBuzzModel) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

       
    }
}
