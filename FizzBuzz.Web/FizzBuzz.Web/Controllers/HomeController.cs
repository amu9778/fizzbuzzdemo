﻿using FizzBuzz.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(FizzBuzzModel fizzBuzzModel)
        {
            if ((fizzBuzzModel.InputNumber < 0 && fizzBuzzModel.InputNumber > 1000))
            {
                ModelState.AddModelError("InputNumber", "Enter a valid Number");
            }
            if (ModelState.IsValid)
            {
                fizzBuzzModel.Records = GetFizzBuzzRecord(fizzBuzzModel.InputNumber);
            }
            return View(model: fizzBuzzModel);
        }

        private List<string> GetFizzBuzzRecord(int inputNumber)
        {
            var records = new List<string>();
            for (int i = 1; i <= inputNumber; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    records.Add("FizzBuzz");
                }
                else
                {
                    if (i % 3 == 0)
                    {
                        records.Add("Fizz");
                    }
                    else if (i % 5 == 0)
                    {
                        records.Add("Buzz");
                    }
                    else
                    {
                        records.Add(i.ToString());
                    }
                }

            }
            return records;
        }
    }
}
