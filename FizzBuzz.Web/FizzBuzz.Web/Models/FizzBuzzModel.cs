﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzModel
    {
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        [Range(1, 1000, ErrorMessage = "The value must be between {1} and {2}")]
        public int InputNumber { get; set; }
        public List<string> Records { get; set; }
    }
}